"""Правая часть системы ОДУ брусселятора"""
function brusselator(t::Float64, x::Vector{Float64})::Vector{Float64}
  return [1 + x[2]*x[1]^2 - 4*x[1], 3*x[1] - x[2]*x[1]^2]
end


"""Правая часть ОДУ линейного осциллятора"""
function oscillator(t::Float64, x::Vector{Float64})::Vector{Float64}
    return [-x[2], x[1]]
end


"""Аналитическое решение ОДУ линейного осциллятора"""
function oscillator_solution(t::Float64, x_0::Vector{Float64})::Vector{Float64}
    return [x_0[1]*cos(t) - x_0[2]*sin(t),
            x_0[2]*cos(t) + x_0[1]*sin(t)]
end


"""Euler's equations (rigid body dynamics)
Hairer, Wanner, Nørsett Solving Ordinary Differential Equations I
page 244
"""
function rigid_body(t::Float64, x::Vector{Float64})::Vector{Float64}
  res = Vector{Float64}(3)
  I1 = -2.0
  I2 = +1.25
  I3 = -0.50
  return [I1*x[2]*x[3], I2*x[1]*x[3], I3*x[1]*x[2]]
end

"""Van der Pol oscillator"""
function vanderpol(μ::Float64)::Function
  return function (t::Float64, x::Vector{Float64})
    return [x[2], μ*(1-x[1]*x[1])*x[2] - x[1]]
  end
end

"""Robertson example for stiff differential equations"""
function robertson(t::Float64, x::Vector{Float64})::Vector{Float64}
  k1 = 0.04
  k2 = 3e7
  k3 = 1e4
  return [ -k1*x[1] + k3*x[2]*x[3], 
    k1*x[1] - k2*x[2]*x[2] - k3*x[2]*x[3],
    k2*x[2]*x[2] ]
end
