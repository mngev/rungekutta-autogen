using RungeKutta
using Test
using Printf

include("equations.jl")

# Список методов. Исключаем :ERK45 и :Fehlberg78A за неадекватность
erk_methods = [:ERK23,:ERK23B,:ERK32,:ERK43,:ERK43B,:DPRK546S,:DPRK547S,:DPRK658M,:Fehlberg45,:CashKarp45,:DOPRI5,:DVERK65,:Fehlberg78B,:DOPRI8]

erk_info_methods = [:ERK23_info,:ERK23B_info,:ERK32_info,:ERK43_info,:ERK43B_info,:DPRK546S_info,:DPRK547S_info,:DPRK658M_info,:Fehlberg45_info,:CashKarp45_info,:DOPRI5_info,:DVERK65_info,:Fehlberg78B_info,:DOPRI8_info]

include("erk_basic_tests.jl")

include("erk_oscillator.jl")
