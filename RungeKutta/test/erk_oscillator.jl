# Линейный осциллятор. При t_stop = 2nπ x_0 = x_(t_end)
x_0 = [1.0, 2.0]
t_start = 0.0
t_stop = 2π
A_tol = 1.0e-13
R_tol = 1.0e-17

@testset "All ERK methods" begin
  for func in erk_methods
    t, x = ERK.eval(func)(oscillator, A_tol, R_tol, x_0, t_start, t_stop, true)
    #@test 0 == 0
    @test hypot(x...) ≈ hypot(x_0...)
    #print_with_color(:red, string(func))
    #print("\nlog(ϵ) = ", round(log(10, err), 2)," ")
  end
end

for i in 2:1:6
  t_stop = 50*i*π
  @printf("t_stop = %iπ\n", 50*i)
  @testset "ERK p >= 6" begin
    for func in [:DPRK658M,:DVERK65,:Fehlberg78B,:DOPRI8]
      t, x = ERK.eval(func)(oscillator, A_tol, R_tol, x_0, t_start, t_stop, true)
      #@test 0 == 0
      @test hypot(x...) ≈ hypot(x_0...)
      #print_with_color(:red, string(func))
      #print("\nlog(ϵ) = ", round(log(10, err), 2)," ")
    end
  end
end
