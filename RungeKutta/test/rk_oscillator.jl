
x_0 = [1.0, 2.0]
t_start = 0.0
t_stop = 2π
h = 0.01

@testset "All ERK methods" begin
  for func in methods
    t, x = ERK.eval(func)(oscillator, A_tol, R_tol, x_0, t_start, t_stop, true)
    #@test 0 == 0
    @test hypot(x...) ≈ hypot(x_0...)
    #print_with_color(:red, string(func))
    #print("\nlog(ϵ) = ", round(log(10, err), 2)," ")
  end
end
