# Проверка правильности логики программы, без оценки точности вычисления
# Линейный осциллятор. При t_stop = 2nπ x_0 = x_(t_end)
x_0 = [1.0, 2.0]
t_start = 0.0
t_stop = 2π
A_tol = 1.0e-5
R_tol = 1.0e-17
# Проверяем есть и нарушения в общей логике работы программы
# 1. Число принятых шагов должно быть на 1 меньше числа точек получившейся
# сетки на отрезке [t, T]
@testset "All ERK methods" begin
  for (func1, func2) in zip(erk_methods, erk_info_methods)
    tn, xn = ERK.eval(func1)(oscillator, A_tol, R_tol, x_0, t_start, t_stop)
    res = ERK.eval(func2)(oscillator, A_tol, R_tol, x_0, t_start, t_stop)
    (accepted_t, accepted_h, rejected_t, rejected_h, accepted_errors, rejected_errors) = res
    @test length(tn) == length(accepted_t) + 1
    @test length(accepted_t) == length(accepted_h)
    @test length(rejected_t) == length(rejected_h)
    if length(accepted_errors) > 0
      @test sum(accepted_errors)/length(accepted_errors) < 1
    end
    if length(rejected_errors) > 0
      @test sum(rejected_errors)/length(rejected_errors) >= 1
  end
  end
end
