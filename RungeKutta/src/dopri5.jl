module dopri5
include("StepControl.jl")

function solve!(func::Function, A_tol::Float64, R_tol::Float64,
               x_start::Vector{Float64},  t_start::Float64, t_stop::Float64,
               T::Vector{Float64}, X::Vector{Vector{Float64}};
               all_values=true)::Bool

  local EQN = length(x_start)
  
  local a21 = 1/5
  local a31 = 3/40
  local a32 = 9/40
  local a41 = 44/45
  local a42 = -56/15
  local a43 = 32/9
  local a51 = 19372/6561
  local a53 = 64448/6561
  local a52 = -25360/2187
  local a54 = -212/729
  local a61 = 9017/3168
  local a62 = -355/33
  local a63 = 46732/5247
  local a64 = 49/176
  local a65 = -5103/18656
  local a71 = 35/384
  local a73 = 500/1113
  local a74 = 125/192
  local a75 = -2187/6784
  local a76 = 11/84

  local  c2 = 1/5
  local  c3 = 3/10
  local  c4 = 4/5
  local  c5 = 8/9
  local  c6 = 1
  local  c7 = 1

  local  b1 = 35/384
  local  b3 = 500/1113
  local  b4 = 125/192
  local  b5 = -2187/6784
  local  b6 = 11/84

  local  E1 = 71/57600
  local  E3 = -71/16695
  local  E4 = 71/1920
  local  E5 = -17253/339200
  local  E6 = 22/525
  local  E7 = -1/40

  local nstiff = 1000

  local h::Float64
  local t::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local error::Vector{Float64}

  local k1 = zeros(Float64, EQN)
  local k2 = zeros(Float64, EQN)
  local k3 = zeros(Float64, EQN)
  local k4 = zeros(Float64, EQN)
  local k5 = zeros(Float64, EQN)
  local k6 = zeros(Float64, EQN)
  local k7 = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  t   = t_start

  accepted = true
  last_step = false

  h = step_init(func, t_start, x_start, A_tol, R_tol, 5, 4)

  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + 0.09*h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # one step
    k1 = func(t, x_0)

    x = x_0 + h*a21*k1
    k2 = func(t + c2*h, x)

    x = x_0 + h*(a31*k1 + a32*k2)
    k3 = func(t + c3*h, x)

    x = x_0 + h*(a41*k1 + a42*k2 + a43*k3)
    k4 = func(t + c4*h, x)

    x = x_0 + h*(a51*k1 + a52*k2 + a53*k3 + a54*k4)
    k5 = func(t + c5*h, x)

    x = x_0 + h*(a61*k1 + a62*k2 + a63*k3 + a64*k4 + a65*k5)
    k6 = func(t + c6*h, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)
    k7 = func(t + c7*h, x)

    error = h*(E1*k1 + E3*k3 + E4*k4 + E5*k5 + E6*k6 + E7*k7)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, error, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end
  return true

end # function solve!

function solve(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64},
               t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64}, Matrix{Float64}}
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  solve!(func, A_tol, R_tol, x_start, t_start, t_stop, T, X, all_values=true)
  return T, transpose(hcat(X...))
end # function solve

end # module dopri5
