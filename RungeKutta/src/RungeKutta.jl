module RungeKutta

export RK, ERK, RRK, dopri5

# Classical Runge-Kutta methods with fixed step size
include("generated/rk.jl")
# Embedded Runge-Kutta methods with step control
include("generated/erk.jl")
# Embedded Rosenbrock-Runge-Kutta methods with step control
include("generated/rrk.jl")

include("euler.jl")

include("CashKarp.jl")
include("dopri5.jl")

end # module
