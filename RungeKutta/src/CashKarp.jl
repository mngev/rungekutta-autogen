function CashKarp45(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64; last::Bool=false)::Tuple{Vector{Float64}, Matrix{Float64}}

  local EQN = length(x_start)

  local a21 = 1/5
  local a31 = 3/40
  local a32 = 9/40
  local a41 = 3/10
  local a42 = -9/10
  local a43 = 6/5
  local a51 = -11/54
  local a52 = 5/2
  local a53 = -70/27
  local a54 = 35/27
  local a61 = 1631/55296
  local a62 = 175/512
  local a63 = 575/13824
  local a64 = 8855/22119
  local a65 = 253/4096

  local b51 = 37/378
  local b53 = 250/621
  local b54 = 125/594
  local b56 = 512/1771

  local b41 = 2825/27648
  local b43 = 18575/48384
  local b44 = 13525/55296
  local b45 = 277/14336
  local b46 = 1/4

  local b31 = 19/54
  local b33 = -10/27
  local b34 = 55/54

  local b21 = -3/2
  local b22 = 5/2

  local b11 = 1.0

  local c2 = 1/5
  local c3 = 3/10
  local c4 = 3/5
  local c5 = 1
  local c6 = 7/8

  local h::Float64
  local t::Float64

  local accepted::Bool
  local last_step::Bool

  local x_0::Vector{Float64}

  local k1, k2, k3, k4, k5, k6 ::Vector{Float64}
  # Промежуточные величины 1,2,3,4,5 го порядков
  local x1, x2, x3, x4, x5 ::Vector{Float64}

  local twiddle1::Float64 = 1.5
  local twiddle2::Float64 = 1.1

  local quit1::Float64 = 100.0
  local quit2::Float64 = 100.0
  local SF = 0.9

  local ESTTOL::Float64
  local Q1, Q2 ::Float64

  local ɛ::Float64

  local ERR1, ERR2, ERR4 ::Float64
  local E1, E2, E4 ::Float64

  x_0 = copy(x_start)
  t   = t_start

  last_step = false
  ɛ = A_tol
  h = 0.01

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x_0)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end
    # println("h = ", h)
    # println("t = ", t)
    # println("x_0 = ", x_0)
    # sleep(0.5)
    # core step

    k1 = func(t, x_0)
    x1  = x_0 + h*b11*k1
    k2 = func(t + h*c2, x_0 + a21*k1)
    x2 = x_0 + h*(b21*k1 + b22*k2)

    ERR1 = norm(x2 - x1)^(1/2)
    E1 = ERR1 / (ɛ^(1/2))

    if E1 > twiddle1 * quit1
      ESTTOL = E1 / quit1
      h = h*max(1/5, SF/ESTTOL)
      last_step = false
      # println(" --- 1")
      continue # abandon
    end

    k3 = func(t + h*c3, x_0 + h*(a31*k1 + a32*k2))
    k4 = func(t + h*c4, x_0 + h*(a41*k1 + a42*k2 + a43*k3))
    x3 = x_0 + h*(k1*b31 + k3*b33 + k4*b34)

    ERR2 = norm(x3 - x2)^(1/3)
    E2 = ERR2 / (ɛ^(1/3))

    if E2 > twiddle2 * quit2
      if E1 < 1
        if h*(k2 - k1)/10 < ɛ # E1n(1/5)
          x_0 = x_0 + h*(k1 + k2)/10 # accept 2 order solution (15)
          h = h/5 # cut the step
          t = t + h
          push!(X, x_0)
          push!(T, t)
          # println(" --- 2")
          continue # accept
        else
          h = h/5
          last_step = false
          # println(" --- 3")
          continue # abandon
        end
      else
        ESTTOL = E2/quit2
        h = h*max(1/5, SF/ESTTOL)
        last_step = false
        # println(" --- 4")
        continue # abandon
      end
    end

    k5 = func(t + h*c5, x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65))

    x4 = x_0 + h*(b41*k1 + b43*k3 + b44*k4 + b45*k5 + b46*k6)
    x5 = x_0 + h*(b51*k1 + b53*k3 + b54*k4 +          b56*k6)

    ERR4 = norm(x5 - x4)^(1/5)
    E4 = ERR4 / (ɛ^(1/5))

    if E4 > 1
      E1/quit1 < twiddle1 ? twiddle1 = max(1.1, E1/quit1) : twiddle1 = twiddle1
      E2/quit2 < twiddle2 ? twiddle2 = max(1.1, E2/quit2) : twiddle2 = twiddle2

      if E2 < 1
        if h*(k1 - 2k3 + k4)/10 < ɛ
          # accept 3 order solution (16)
          x_0 = x_0 + h * (0.1*k1 + 2*k3/5 + 0.1*k4)
          h = 3*h/5
          t = t + h
          push!(X, x_0)
          push!(T, t)
          # println(" --- 5")
          continue # accept
        end
      else
        if E1 < 1
          if h*(k2 - k1)/10 < ɛ # E1n(1/5)
            # accept 2 order solution (15)
            x_0 = x_0 + h*(k1 + k2)/10
            h = h/5 # cut the step
            t = t + h
            push!(X, x_0)
            push!(T, t)
            # println(" --- 6")
            continue # accept
          else
            h = h/5
            last_step = false
            println(" --- 7")
            continue # abandon
          end
        else
          ESTTOL = E4
          h = h*max(1/5, SF/ESTTOL)
          last_step = false
          # println(" --- 8")
          continue # abandon
        end
      end
    else
      # accept order 5
      x_0 = x5
      t = t + h
      h = h*min(5.0, SF/E4)
      push!(X, x_0)
      push!(T, t)
      Q1 = E1/E4
      Q2 = E2/E4

      Q1 > quit1 ? Q1 = min(Q1, 10*quit1) : Q1 = max(Q1,  2*quit1/3)
      Q2 > quit2 ? Q2 = min(Q2, 10*quit2) : Q2 = max(Q2,  2*quit2/3)

      quit1 = max(1.0, min(1e4, Q1))
      quit2 = max(1.0, min(1e4, Q2))

      twiddle1 = twiddle1
      twiddle2 = twiddle2
      # println(" --- 9")
    end
    # println(" --- 10")
  end

  return(T, transpose(hcat(X...)))
end # function CashKarp45
