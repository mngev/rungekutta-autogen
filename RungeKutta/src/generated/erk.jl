module ERK

include("../StepControl.jl")

functions = [:ERK23,:ERK23B,:ERK32,:ERK43,:ERK43B,:ERK45,:DPRK546S,:DPRK547S,:DPRK658M,:Fehlberg45,:CashKarp45,:DOPRI5,:DVERK65,:Fehlberg78A,:Fehlberg78B,:DOPRI8]

info_functions = [:ERK23_info,:ERK23B_info,:ERK32_info,:ERK43_info,:ERK43B_info,:ERK45_info,:DPRK546S_info,:DPRK547S_info,:DPRK658M_info,:Fehlberg45_info,:CashKarp45_info,:DOPRI5_info,:DVERK65_info,:Fehlberg78A_info,:Fehlberg78B_info,:DOPRI8_info]

include("erk_base.jl")
include("erk_info.jl")


end # module ERK