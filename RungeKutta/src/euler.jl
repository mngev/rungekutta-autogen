"""
Явный метод Эйлера.

`euler(func, x_0, h, time_interval) -> (T, X)`

# Аргументы

* `func::Function`: правая часть ОДУ.
* `x_0::Vector{Float64}`: начальное значения переменной `x`.
* `time_interval::Tuple{Float64}`: начальное и конечное значение отрезка интегрирования.
* `h::Float64`: текущий шаг метода.

# Возвращаемые значения

* `T::Array{Float64, 1}`: временные точки из отрезка `time_interval`, временная сетка равномерная.
* `X::Array{Float64, EQN}`: численное решение в виде массива c `EQN` столбцов и `N` строк.

"""
function euler(func::Function, x_0::Array{Float64, 1}; h=0.1, time_interval=(0.0, 1.0))
  eqn = length(x_0)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()

  local t::Float64 = time_interval[1]
  local x::Vector{Float64} = copy(x_0)
  local last_step::Bool = false
  
  while true
    push!(T, t)
    push!(X, x)
    
    x = x + h * func(t, x)
    
    last_step && break
    
    if t + h >= time_interval[2]
      h = time_interval[2] - t
      last_step = true
    end
    t += h
  end
  return T, transpose(hcat(X...))
end

function euler(func::Function, x_0::Float64; h=0.1, time_interval=(0.0, 1.0))
  eqn = length(x_0)

  local X = Vector{Float64}()
  local T = Vector{Float64}()

  local t::Float64 = time_interval[1]
  local x::Float64 = x_0
  local last_step::Bool = false
  
  while true
    push!(T, t)
    push!(X, x)
    
    x = x + h * func(t, x)
    
    last_step && break
    
    if t + h >= time_interval[2]
      h = time_interval[2] - t
      last_step = true
    end
    t += h
  end
  return T, X
end