mutable struct StepControlState
  "Factor min"
  f_min::Float64
  "Factor max"
  f_max::Float64
  "Error from previous step"
  E0::Float64
  "PI controller coefficient"
  α::Float64
  "PI controller coefficient"
  β::Float64
  "safety factor"
  save::Float64
  q::Int64
end
const SCS = StepControlState(0.1, 5.0, 1.0e-4, 0.1, 0.08, 0.9, 4)

"""Root mean square function"""
function rms(x::Vector{Float64})::Float64
  local res:: Float64
  res = sum(x.^2) / length(x)
  res = sqrt(res)
  return res
end

"""
Выбор начального значения шага `h`
`init_step(func, t_0, x_0, A_tol, R_tol, method_order) -> h_0`

# Аргументы

* `func::Function`: правая часть ОДУ.
* `t_0::Float64`: начальное время.
* `x_0::Vector{Float64}`: Начальное значение переменной.
* `A_tol::Float64`: желаемая абсолютная погрешность.
* `R_tol::Float64`: желаемая относительная погрешность.
* `p::Int64`: порядок метода
* `p_hat::Int64`: порядок вложенного метода

# Возвращаемые значения

* `h_0::Float64`: найденный начальный шаг.
"""
function step_init(func::Function, t_0::Float64, x_0::Vector{Float64}, A_tol::Float64, R_tol::Float64, p::Int64, p_hat::Int64)::Float64
  global SCS
  SCS.f_min = 0.1
  SCS.f_max = 5.0
  SCS.E0 = 1.0e-4
  SCS.β = 0.4/p
  SCS.α = 1/p - 0.75*SCS.β
  SCS.save = 0.9
  q = min(p, p_hat) + 1
  SCS.q = q

  # Старт алгоритма выбора начальной величины шага
  scale = A_tol .+ abs.(x_0) .* R_tol
  # Ориентируемся на текст программы DORPI5
  d_0 = rms(x_0 ./ scale)
  f1 = func(t_0, x_0)
  d_1 = rms(f1 ./ scale)

  if d_0 < 1.0e-5 || d_1 < 1.0e-5
    h_0 = 1.0e-6
  # Надо учесть случай, когда d_1 равен нулю
  elseif d_1 >= eps(Float64)
    h_0 = 0.01 * d_0 / d_1
  else
    h_0 = 0.01 * d_0 / eps(Float64)
  end
  # Делаем один шаг явного метода Эйлера
  x_1 = x_0 + h_0 .* func(t_0, x_0)
  f2 = func(t_0 + h_0, x_1)

  d_2 = rms((f2 - f1) ./ scale)
  d_2 = d_2 / h_0

  if max(d_1, d_2) <= eps(Float64)
    h_1 = max(1.0e-6, h_0*1.0e-3)
  else
    h_1 = (0.01 / max(d_1, d_2))^(1/q)
  end
  return min(100*h_0, h_1)
end

"""
Управление шагом.
`step_control(x_0, x, error, t, h, A_tol, R_tol) -> t, h, x_0, reject`

Оценивает погрешность и в зависимости от результата результат
текущей итерации или принимается или отбраковывается и итерация повторяется
заново с новым значением `h`.

# Аргументы

* `x_0::Vector{Float64}`: значение переменной, вычисленное на предыдущем шаге; оно является начальным значением для текущего шаге.
* `x::Vector{Float64}`: значение переменной, вычисленное на текущем шаге.
* `Δ::Vector{Float64}`: error value = x - x_hat.
* `t::Float64`: начальное время на текущем шаге.
* `h::Float64`: шаг метода, с которым были вычислены x и x_hat.
* `A_tol::Float64`: желаемая абсолютная погрешность.
* `R_tol::Float64`: желаемая относительная погрешность.

# Возвращаемые значения

* `t::Float64`: следующий момент времени; если шаг отброшен, то `t = t`, если принят, то `t = t + h`.
* `h::Float64`: величина нового шага.
* `x_0::Float64`: новое начальное значение, если шаг отброшен, то `x_0 = x_0`, если принят, то `x_0 = x`.
* `step_rejected::Bool`: если шаг отброшен, то `true`, если принят, то `false`.
"""
function step_control(x_0::Vector{Float64}, x::Vector{Float64}, Δ::Vector{Float64}, h_old::Float64, A_tol::Float64, R_tol::Float64)::Tuple{Bool, Float64, Float64}

  local accepted::Bool
  local scale::Vector{Float64}
  # Current step error value
  local E1::Float64
  local h_new::Float64
  local t_new::Float64
  local factor::Float64
  global SCS

  scale = A_tol .+ max.(abs.(x_0), abs.(x)) .* R_tol

  E1 = rms(Δ ./ scale)

  factor = (E1^SCS.α) / (SCS.E0^SCS.β)
  factor = max(SCS.f_min, min(SCS.f_max, factor/SCS.save))

  h_new = h_old / factor

  # Step accepted
  if E1 <= 1.0
    SCS.E0 = max(E1, 1.0e-4)
    accepted = true
  # Step rejected
  else
    h_new = h_old / min(SCS.f_max, (E1^SCS.α)/SCS.save)
    accepted = false
  end
  return (accepted, h_new, E1)
end
