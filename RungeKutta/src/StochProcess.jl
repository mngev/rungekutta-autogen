module StochProcess

include("random.jl")


abstract type AbstractStochasticProcess end

struct StochasticProcess <: AbstractStochasticProcess
    N::Int64
    t_0::Float64
    t_N::Float64
    Δt::Float64
    T::Vector{Float64}
    X::Vector{Float64}

    function StochasticProcess(N::Int64, t_0::Float64, t_N::Float64)
        Δt = (t_N - t_0)/N
        T = Vector(linspace(t_0, t_N, N))
        X = zeros(T)
        return new(N, t_0, t_N, Δt, T, X)
    end
end

function StochasticProcess(N::Int64, Δt::Float64)
    return StochasticProcess(N, 0.0, Δt * N)
end

function StochasticProcess(N::Int64)
    return StochasticProcess(N, 0.0, 1.0)
end

function StochasticProcess()
    return StochasticProcess(1000, 0.0, 1.0)
end

struct WienerProcess <: AbstractStochasticProcess
    N::Int64
    t_0::Float64
    t_N::Float64
    Δt::Float64
    T::Vector{Float64}
    X::Vector{Float64}
    dX::Vector{Float64}

    function WienerProcess(N::Int64, t_0::Float64, t_N::Float64)
        Δt = (t_N - t_0)/N
        T = Vector(linspace(t_0, t_N, N))
        # можно использовать встроенную функцию, генерирующую нормальное
        # распределение
        # dX = sqrt(Δt) .* randn(N)
        dX = sqrt(Δt) .* Random.normal(N, 0.0, 1.0)
        t_0 == 0 && (dX[1] = 0.0)
        X = cumsum(dX)
        return new(N, t_0, t_N, Δt, T, X, dX)
    end
end

function WienerProcess(N::Int64, Δt::Float64)
    return WienerProcess(N, 0.0, Δt * N)
end

function WienerProcess(N::Int64)
    return WienerProcess(N, 0.0, Δt * N)
end

function WienerProcess()
    return WienerProcess(100, 0.0, 1.0)
end

end