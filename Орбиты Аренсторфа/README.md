# Расчет и визуализация трех орбит Аренсторфа

## Вычисления
- В файле `equations.jl` заданы необходимые функции и три группы начальных значений.
- В файле `arenstorf.jl` расчеты для визуализации орбит одним каким-нибудь методом (все вложенные справляются корректно). Генерирует CSV файлы по одному на каждую из трех орбит.
- В файле `arenstorf_error.jl` вычисление абсолютной глобальной погрешности для расчетов одного оборота по орбите разными методами. Работает для первой орбиты, так как для остальных не подобрано точное время прохождения начальной точки. Генерирует текст для LaTeX таблиц.

## CSV таблицы

Расчеты сохраняются в виде CSV файлов. Следующие колонки:
1. Время.
2. Координата $q_x$.
3. Координата $q_y$.

## Визуализация (проще говоря рисование)

- Рисование с помощью `Asymptote`
- В `config.asy` надо задать такую же преамбулу, как и у основного документа, куда будет вставляться картинка, тогда шрифты будут идентичны.
- Скрипт `arenstorf.asy` получает данные из STDIN в виде CSV таблицы и использует 2 и 3 колонки для рисования орбиты.
- Также отмечаются большое и среднее тела (малое тело с нулевой массой крутится вокруг них).
