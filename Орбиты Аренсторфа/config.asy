import settings;
prc = false;
outformat = "pdf";
tex = "lualatex";
// Включает/отключает локаль (например, десятичный разделитель --- запятая)
locale("ru_RU.UTF-8");
texpreamble("
\usepackage{polyglossia}
\setdefaultlanguage[indentfirst=true,spelling=modern]{russian}
\setotherlanguage{english}

\usepackage{fontspec}
%\usepackage{xunicode}
%\usepackage{xltxtra}

\setmainfont[Ligatures=TeX,Scale=MatchLowercase]{CMU Serif}
\setsansfont[Ligatures=TeX,Scale=MatchLowercase]{CMU Sans Serif}
\setmonofont[Scale=MatchLowercase]{CMU Typewriter Text}

% CMU Serif
\newfontfamily\cyrillicfont%
[Script=Cyrillic,Ligatures=TeX,Scale=MatchLowercase]%
{CMU Serif}

% CMU Sans Serif
\newfontfamily\cyrillicfontsf%
[Script=Cyrillic,Ligatures=TeX,Scale=MatchLowercase]%
{CMU Sans Serif}

% CMU Typewriter Text
\newfontfamily\cyrillicfonttt%
[Script=Cyrillic,Scale=MatchLowercase]%
{CMU Typewriter Text}

\usepackage{unicode-math}
% Нужно, если включена локаль, иначе какая-то беда
% с сеткой на графиках см. https://asymptote.sourceforge.io/FAQ/section4.html#decsep
\usepackage{icomma}
");