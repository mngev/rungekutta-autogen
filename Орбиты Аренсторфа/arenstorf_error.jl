push!(LOAD_PATH, "../")
using RungeKutta
using Printf
using LinearAlgebra

include("equation.jl")

# Вычисляем погрешности
@printf("Метод & Погрешность\\\\\n")
for func in (:DPRK546S, :DPRK547S, :DPRK658M, :Fehlberg45, :DOPRI5, :DVERK65, :Fehlberg78B, :DOPRI8)
  tn, xn = ERK.eval(func)(arenstorf, A_tol, R_tol, x_0, t_start, t_stop)
  @printf("%s & %g\\\\\n", func, norm(xn[1,3:4] - xn[end,3:4]))
end