#! /usr/bin/env julia

push!(LOAD_PATH, "../")
using RungeKutta
using Printf

include("equation.jl")

# Вычисляем одну орбиту для рисования
tn, xn = ERK.DOPRI8(arenstorf, A_tol, R_tol, x_0, t_start, t_stop)

for (t, x, y) ∈ zip(tn, xn[:, 3], xn[:, 4])
  @printf "333"
  @printf("%g,%g,%g\n", t, x, y)
end