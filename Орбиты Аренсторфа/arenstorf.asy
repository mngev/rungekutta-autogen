include "./config.asy";

import graph;

size(250,250);

// Границы осей
xlimits(-1, 1);
ylimits(-1, 1);

// Считывание данных из csv файла по столбцам
file in = input(comment="#", mode="");

real[][] data = in.line().csv();

data = transpose(data);

// Орбиты
path xy = graph(data[1], data[2]);
draw(g=xy, p=black+1bp);

// Настройка сетки
pen thin = linewidth(0.25bp)+grey;
ticks OX = Ticks(pTick=thin, ptick=thin+dashed, extend=true);
ticks OY = Ticks(pTick=thin, ptick=thin+dashed, extend=true);

// подписи к осям
xaxis(L="$q^x(t)$", axis=BottomTop, ticks=OX);
yaxis(L="$q^y(t)$", axis=LeftRight, ticks=OY);

// Центральное тело mu_2
dot((0,0), p=3bp+black, L="$\mu_2$", align=N);

// Начальная точка == среднее тело
dot((data[1][0],data[2][0]), p=3bp+grey, L="$\mu_1$");